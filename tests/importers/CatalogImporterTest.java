package importers;

import importers.exceptions.ImportingException;
import importers.models.catalog.Book;
import importers.models.catalog.Catalog;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class CatalogImporterTest {

    private Importer<Catalog> catalogImporter;

    public CatalogImporterTest(Importer<Catalog>  catalogImporter) {
        this.catalogImporter = catalogImporter;
    }

    @Parameterized.Parameters(name = "{0}") // {0} = toString() of object
    public static java.util.Collection<Object[]> generateData() throws ImportingException {
        return Arrays.asList(new Object[][]{
                {new CatalogImporter()},
                {new CatalogImporterVersionTwo()},
        });
    }

    @Test
    public void shouldImportXML() throws ImportingException {
        Catalog catalog = executeImporter();
        int numberOfBooks = catalog.getBooks().size();
        Assert.assertEquals(12, numberOfBooks);
    }

    @Test
    public void fieldsShouldNotBeNull() throws ImportingException {
        Catalog catalog = executeImporter();
        List<Book> books = catalog.getBooks();

        for (Book book : books) {
            Object bookFields[] = {
                    book.getAuthor(),
                    book.getDescription(),
                    book.getGenre(),
                    book.getId(),
                    book.getPrice(),
                    book.getPublish_date(),
                    book.getTitle()
            };
            for (Object bookField : bookFields) {
                Assert.assertNotNull(bookField);
            }
        }
    }

    private Catalog executeImporter() throws ImportingException {
        return catalogImporter.execute();
    }
}
