package importers.converters;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import xml.Parser;
import xml.exceptions.ParsingException;
import xml.exceptions.XMLParsingException;
import xml.jaxb.JaxbXmlParser;
import xml.simpleXml.SimpleXmlParser;

import java.net.URL;
import java.util.Arrays;

@SuppressWarnings("unchecked") // Unable to make type of converter<> explicit because of parameterized runner.
@RunWith(Parameterized.class)
public class ConvertersTest {

    private final Parser parser;
    private final Class catalogClass;
    private final Converter converter;

    public ConvertersTest(Parser parser, Class catalogClass, Converter converter) {
        this.parser = parser;
        this.catalogClass = catalogClass;
        this.converter = converter;
    }

    @Parameterized.Parameters(name = "{0}") // {0} = toString() of object
    public static java.util.Collection<Object[]> generateData() throws XMLParsingException {
        return Arrays.asList(new Object[][]{
                {new JaxbXmlParser(), xml.jaxb.models.Catalog.class, new JaxbCatalogConverter()},
                {new SimpleXmlParser(), xml.simpleXml.models.Catalog.class, new SimpleXmlCatalogConverter()},
        });
    }


    @Test
    public void convertToCatalog() throws ParsingException {
        Object catalog = parseXml();
        converter.convert(catalog);
    }

    private Object parseXml() throws ParsingException {
        URL resourceURL = this.getClass().getResource("/books.xml");
        return catalogClass.cast(parser.parse(resourceURL, catalogClass));
    }
}
