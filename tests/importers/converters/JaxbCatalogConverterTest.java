package importers.converters;

import importers.models.catalog.Catalog;
import org.junit.Before;
import org.junit.Test;
import xml.exceptions.XMLParsingException;
import xml.jaxb.JaxbXmlParser;

import java.net.URL;

public class JaxbCatalogConverterTest {

    private JaxbCatalogConverter jaxbCatalogConverter;
    private JaxbXmlParser jaxbXmlParser;

    @Before
    public void setup() throws XMLParsingException {
        this.jaxbCatalogConverter = new JaxbCatalogConverter();
        this.jaxbXmlParser = new JaxbXmlParser();
    }

    private xml.jaxb.models.Catalog getParsedXMLModel() throws XMLParsingException {
        URL resourceURL = this.getClass().getResource("/books.xml");
        return jaxbXmlParser.parse(resourceURL, xml.jaxb.models.Catalog.class);
    }

    @Test
    public void convertToCatalog() throws XMLParsingException {
        Catalog convert = jaxbCatalogConverter.convert(getParsedXMLModel());
    }
}
