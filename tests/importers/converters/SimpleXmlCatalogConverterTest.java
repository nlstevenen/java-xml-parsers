package importers.converters;

import org.junit.Before;
import org.junit.Test;
import xml.exceptions.XMLParsingException;
import xml.simpleXml.SimpleXmlParser;

import java.net.URL;

public class SimpleXmlCatalogConverterTest {

    private SimpleXmlCatalogConverter simpleXmlCatalogConverter;
    private SimpleXmlParser simpleXmlParser;

    @Before
    public void setup() throws XMLParsingException {
        this.simpleXmlCatalogConverter = new SimpleXmlCatalogConverter();
        this.simpleXmlParser = new SimpleXmlParser();
    }

    private xml.simpleXml.models.Catalog getParsedXMLModel() throws XMLParsingException {
        URL resourceURL = this.getClass().getResource("/books.xml");
        return simpleXmlParser.parse(resourceURL, xml.simpleXml.models.Catalog.class);
    }

    @Test
    public void convertToCatalog() throws XMLParsingException {
        simpleXmlCatalogConverter.convert(getParsedXMLModel());
    }
}
