package xml;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import xml.exceptions.ParsingException;
import xml.exceptions.XMLParsingException;
import xml.jaxb.JaxbXmlParser;
import xml.simpleXml.SimpleXmlParser;

import java.net.URL;
import java.util.Arrays;

@RunWith(Parameterized.class)
public class XmlParsersTest {

    private final Parser parser;
    private final Class catalogClass;

    public XmlParsersTest(Parser parser, Class catalogClass) {
        this.parser = parser;
        this.catalogClass = catalogClass;
    }

    @Parameterized.Parameters(name = "{0}") // {0} = toString() of object
    public static java.util.Collection<Object[]> generateData() throws XMLParsingException {
        return Arrays.asList(new Object[][]{
                {new JaxbXmlParser(), xml.jaxb.models.Catalog.class},
                {new SimpleXmlParser(), xml.simpleXml.models.Catalog.class},
        });
    }
    @SuppressWarnings("unchecked")
    @Test
    public void parseWithoutExceptions() throws ParsingException {
        URL resourceURL = this.getClass().getResource("/books.xml");
        Object parsingResult = catalogClass.cast(parser.parse(resourceURL, catalogClass));
    }
}
