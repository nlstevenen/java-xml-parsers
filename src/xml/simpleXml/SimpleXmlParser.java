package xml.simpleXml;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import xml.Parser;
import xml.exceptions.XMLParsingException;

import java.io.File;
import java.net.URL;

public class SimpleXmlParser implements Parser {
    private final Serializer serializer;

    public SimpleXmlParser() {
        serializer = new Persister();
    }

    @Override
    public <T> T parse(URL resourceURL, Class<T> returnClass) throws XMLParsingException {
        try {
            File pathToXmlFile = new File(resourceURL.getPath());
            return serializer.read(returnClass, pathToXmlFile);
        } catch (Exception e) {
            throw new XMLParsingException("SimpleXml parser failed.", e);
        }
    }
}
