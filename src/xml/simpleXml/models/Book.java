package xml.simpleXml.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

import java.math.BigDecimal;

public class Book {

    @Attribute(name = "id")
    public String id;
    @Element
    private String author;
    @Element
    private String title;
    @Element
    private String genre;

    @Element
    private BigDecimal price;
    @Element
    private String publish_date;
    @Element
    private String description;

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public String getDescription() {
        return description;
    }
}
