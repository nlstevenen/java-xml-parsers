package xml.exceptions;

public class XMLParsingException extends ParsingException {
    public XMLParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
