package xml;

import xml.exceptions.ParsingException;

import java.net.URL;

public interface Parser {

    public <T> T parse(URL resourceURL, Class<T> returnClass) throws ParsingException;
}
