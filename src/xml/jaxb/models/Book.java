package xml.jaxb.models;

import xml.jaxb.adapters.DateAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDate;

@SuppressWarnings(value = "UnusedDeclaration")
public class Book {

    @XmlAttribute(name = "id")
    public String id;
    @XmlElement
    private String author;
    @XmlElement
    private String title;
    @XmlElement
    private String genre;

    @XmlElement
    private BigDecimal price;
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    private LocalDate publish_date;
    @XmlElement
    private String description;

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDate getPublish_date() {
        return publish_date;
    }

    public String getDescription() {
        return description;
    }

}
