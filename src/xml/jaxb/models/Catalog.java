package xml.jaxb.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Catalog {


    private List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    @XmlElement(name = "book")
    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
