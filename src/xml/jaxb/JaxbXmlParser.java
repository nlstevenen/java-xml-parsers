package xml.jaxb;

import xml.Parser;
import xml.exceptions.XMLParsingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.net.URL;

public class JaxbXmlParser implements Parser {
    private JAXBContext jaxbContext;
    private Unmarshaller jaxbUnmarshaller;

    @Override
    public <T> T parse(URL resourceURL, Class<T> returnClass) throws XMLParsingException {
        try {
            initializeJaxbParser(returnClass);
            return returnClass.cast(jaxbUnmarshaller.unmarshal(resourceURL));
        } catch (JAXBException e) {
            throw new XMLParsingException("jaxb Parser failed while parsing", e);
        }
    }

    private void initializeJaxbParser(Class returnClass) throws XMLParsingException {
        try {
            jaxbContext = JAXBContext.newInstance(returnClass);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            throw new XMLParsingException("Could not instantiate jaxb parser.", e);
        }
    }
}
