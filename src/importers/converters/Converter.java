package importers.converters;

public interface Converter<FROM, TO> {

    public TO convert(FROM from);
}
