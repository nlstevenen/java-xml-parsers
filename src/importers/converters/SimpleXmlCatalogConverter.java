package importers.converters;

import importers.models.catalog.Book;
import importers.models.catalog.Catalog;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class SimpleXmlCatalogConverter implements Converter<xml.simpleXml.models.Catalog, Catalog> {

    @Override
    public Catalog convert(xml.simpleXml.models.Catalog catalog) {

        List<Book> books = new ArrayList<>();

        for (xml.simpleXml.models.Book book : catalog.getBooks()) {
            books.add(convertBook(book));
        }
        return new Catalog(books);
    }

    public Book convertBook(xml.simpleXml.models.Book book) {
        LocalDate publish_date = LocalDate.parse(book.getPublish_date());
        return new Book(book.getId(), book.getAuthor(), book.getTitle(), book.getGenre(), book.getPrice(), publish_date, book.getDescription());
    }
}
