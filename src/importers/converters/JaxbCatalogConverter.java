package importers.converters;

import importers.models.catalog.Book;
import importers.models.catalog.Catalog;

import java.util.ArrayList;
import java.util.List;


public class JaxbCatalogConverter implements Converter<xml.jaxb.models.Catalog, Catalog> {

    @Override
    public Catalog convert(xml.jaxb.models.Catalog catalog) {

        List<Book> books = new ArrayList<>();

        for (xml.jaxb.models.Book book : catalog.getBooks()) {
            books.add(convertBook(book));
        }
        return new Catalog(books);
    }

    public Book convertBook(xml.jaxb.models.Book book) {
        return new Book(book.getId(), book.getAuthor(), book.getTitle(), book.getGenre(), book.getPrice(), book.getPublish_date(), book.getDescription());
    }
}
