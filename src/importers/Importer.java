package importers;

import importers.exceptions.ImportingException;

public interface Importer<T> {

    public T execute() throws ImportingException;
}
