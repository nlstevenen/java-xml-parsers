package importers;

import importers.converters.Converter;
import importers.converters.SimpleXmlCatalogConverter;
import importers.exceptions.ImportingException;
import importers.models.catalog.Catalog;
import xml.Parser;
import xml.exceptions.ParsingException;
import xml.simpleXml.SimpleXmlParser;

import java.net.URL;


/**
 * When CatalogImporter would be refactored to use a different implementation: SimpleXml parser instead of Jaxb parser.
 * Then this class, and only this class will be changed; parser and converter are encapsulated / decoupled from the rest of the application.
 */
public class CatalogImporterVersionTwo implements Importer<Catalog> {

    private final Parser parser;
    private final Converter<xml.simpleXml.models.Catalog, Catalog> converter;
    private final String path;

    public CatalogImporterVersionTwo() throws ImportingException {
        this.parser = initializeParser();
        this.converter = new SimpleXmlCatalogConverter();
        this.path = "/books.xml";
    }

    private SimpleXmlParser initializeParser() throws ImportingException {
        return new SimpleXmlParser();
    }

    public Catalog execute() throws ImportingException {
        URL resourceURL = getURLToResource();
        xml.simpleXml.models.Catalog parsedXML = executeParser(resourceURL, xml.simpleXml.models.Catalog.class);
        return converter.convert(parsedXML);
    }

    private <ReturnType> ReturnType executeParser(URL resourceURL, Class<ReturnType> xmlModelClass) throws ImportingException {
        try {
            return parser.parse(resourceURL, xmlModelClass);
        } catch (ParsingException e) {
            throw new ImportingException("Executing parser failed.", e);
        }
    }

    private URL getURLToResource() {
        return this.getClass().getResource(path);
    }
}
