package importers.exceptions;

public class ImportingException extends  Exception {
    public ImportingException(String message, Throwable cause) {
        super(message, cause);
    }
}
