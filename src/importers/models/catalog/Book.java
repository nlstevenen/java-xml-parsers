package importers.models.catalog;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Book {
    private final String id;
    private final String author;
    private final String title;
    private final String genre;
    private final BigDecimal price;
    private final LocalDate publish_date;
    private final String description;

    public Book(String id, String author, String title, String genre, BigDecimal price, LocalDate publish_date, String description) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.genre = genre;
        this.price = price;
        this.publish_date = publish_date;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDate getPublish_date() {
        return publish_date;
    }

    public String getDescription() {
        return description;
    }

}
