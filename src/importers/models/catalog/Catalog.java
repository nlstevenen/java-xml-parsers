package importers.models.catalog;

import java.util.List;

public class Catalog {

    private final List<Book> books;

    public Catalog(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }
}
