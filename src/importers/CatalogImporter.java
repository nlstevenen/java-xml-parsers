package importers;

import importers.converters.Converter;
import importers.converters.JaxbCatalogConverter;
import importers.exceptions.ImportingException;
import importers.models.catalog.Catalog;
import xml.Parser;
import xml.exceptions.ParsingException;
import xml.jaxb.JaxbXmlParser;

import java.net.URL;

public class CatalogImporter implements Importer<Catalog> {

    private final Parser parser;
    private final Converter<xml.jaxb.models.Catalog, Catalog> converter;
    private final String path;

    public CatalogImporter() throws ImportingException {
        this.parser = initializeParser();
        this.converter = new JaxbCatalogConverter();
        this.path = "/books.xml";
    }

    private JaxbXmlParser initializeParser() throws ImportingException {
        return new JaxbXmlParser();
    }

    public Catalog execute() throws ImportingException {
        URL resourceURL = getURLToResource();
        xml.jaxb.models.Catalog parsedXML = executeParser(resourceURL, xml.jaxb.models.Catalog.class);
        return converter.convert(parsedXML);

    }

    private <ReturnType> ReturnType executeParser(URL resourceURL, Class<ReturnType> xmlModelClass) throws ImportingException {
        try {
            return parser.<ReturnType>parse(resourceURL, xmlModelClass);
        } catch (ParsingException e) {
            throw new ImportingException("Executing parser failed.", e);
        }
    }

    private URL getURLToResource() {
        return this.getClass().getResource(path);
    }
}
